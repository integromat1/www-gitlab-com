---
title: "GitLab's Functional Group Updates - June"
author: Trevor Knudsen
author_gitlab: tknudsen
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on in June"
canonical_path: "/blog/2018/06/01/functional-group-updates/"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Friday, right before our GitLab Team call, a different Functional Group gives an update to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->


----

### Frontend Team

[Presentation slides](https://docs.google.com/presentation/d/1BVgJAcYj9D_2lL8Es6W_kdNtdIqMwG-GdIDL8tCem6U/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/XogBTBoKpAc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Quality Team

[Presentation slides](https://docs.google.com/presentation/d/1uBEvxPeX8lHF_Un4KgGz9mROh5oBN0k0sXApcHPTq-E/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/6XO0xKDPKF8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### UX Research

[Presentation slides](https://docs.google.com/presentation/d/1XhN8XSQGfOj47gubboMeGm_5IcX_Z13uC2-yCIyV0s4/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/zk4ahAQEbO8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Engineering Team

[Presentation slides](https://docs.google.com/presentation/d/1Sh7HKIU3uGN9M9X9wry6SCiL3upimFDoQzZPVu6D0gY/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/NuIZXy4qTro" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### UX Team

[Presentation slides](https://docs.google.com/presentation/d/1mZHbyBVGfl0xoOe1sTsaZsSihiTczUti5GG-4d0iclY/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/KZJpvfVlQc4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!

----
