---
layout: markdown_page
title: "Improve Ops Quality"
description: "Work on critical test gaps to mitigate future incidents and empower developers to efficiently work on test coverage"
canonical_path: "/company/team/structure/working-groups/improve-ops-quality/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | March 3, 2021   |
| Target End Date | TBD             |
| Slack           | [#wg_improve-ops-quality](https://join.slack.com/share/zt-mvpz7iqd-JHTWucxR3YiCayWM~A25Vg) (only accessible from within the company) | 
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/11iNJ9-KslGfDr6NtVeimLNSa1kWK_2k4uc2wxS4Baw4/edit) (only accessible from within the company) | 
| Task Board      | [Issue board](https://gitlab.com/groups/gitlab-org/-/boards/2448760) |

## Business Goal

Mitigate future incidents by empowering developers to efficiently work on test coverage; identifying and addressing missing test infrastructure and testing categories; and filling test gaps in high-risk product categories.

### Entry Criteria
1. Identify [high-risk product groups](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/76328).

### Exit Criteria
1. Analyze all high-severity incidents in the past 3 months and list key test themes (e.g. mixed deployments, new staging, load testing, better tests outside unit tests).
1. Identify high-risk product categories and missing test infrastructure (e.g. load testing evironment, mixed deployments).
1. Identify test infrastructure or documentation improvements to reduce friction when contributing to test coverage.
1. Outline a plan to add tests to high-risk areas and build missing test infrastructure.
1. Add new validation points before deployment (new gates) for Runner and Package groups.
1. Incorporate this into the process, documentation, on-boarding, and culture going forward.

### Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Mek Stittri           | Director of Quality            |
| Facilitator           | Joanna Shih           | Quality Engineering Manager, Ops |
| Functional Lead       | Sofia Vistas          | Software Engineer in Test, Package:Package |
| Functional Lead       | Tiffany Rea           | Software Engineer in Test, Verify:Continuous Integration |
| Functional Lead       | Zeff Morgan           | Software Engineer in Test, Verify:Runner |
| Stakeholder           | Christopher Lefelhocz | VP of Development              |
| Stakeholder           | Brent Newton          | Director of Infrastructure, Reliability |
| Member                | Kenny Johnston        | Sr. Director of Product Management, Ops |
| Member                | Sam Goldstein         | Director of Engineering, Ops   |
| Member                | Jackie Porter         | Group Manager, Product, Verify   |
| Member                | Dan Croft             | Sr. Manager, Engineering, Continuous Delivery |
| Member                | Darby Frey            | Sr. Manager, Engineering, Verify |
| Member                | Cheryl Li             | Backend Engineering Manager, Verify:Continuous Integration |
| Member                | Elliot Rushton        | Backend Engineering Manager, Verify:Runner |
| Member                | Tanya Pazitny         | Quality Engineering Manager, Secure & Enablement |
| Member                | Thong Kuah            | Staff Backend Engineer, Configure |
