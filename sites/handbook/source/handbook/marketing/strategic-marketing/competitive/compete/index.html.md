---
layout: handbook-page-toc
title: "Competitive Intelligence Resources"
noindex: true
---


# **THIS PAGE HAS BEEN DEPRECATED**

##### **All our product comparison pages are available at [DevOps Tools Comparison Pages](https://about.gitlab.com/devops-tools/).**

For competitive research requests, please use this [Issue Template](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#).

Here are some quick links to find the information that was previously in this page.

- [GitHub](https://about.gitlab.com/devops-tools/github-vs-gitlab.html#github-to-gitlab-decision-kit)
- [Azure DevOps](https://about.gitlab.com/devops-tools/azure-devops-vs-gitlab.html#azure-devops-to-gitlab-decision-kit)
- [Jenkins](https://about.gitlab.com/devops-tools/jenkins-vs-gitlab.html#jenkins-to-gitlab-decision-kit)
- [CircleCI](https://about.gitlab.com/devops-tools/circle-ci-vs-gitlab.html#circleci-to-gitlab-decision-kit)

**DEPRECATED -~~Welcome to the new competitive intelligence resources page!~~**
